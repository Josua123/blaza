package com.gamecodeschool.blaza;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RESTClient {
    private static Blaza_API REST_CLIENT;

    static {
        setupRestClient();
    }

    private RESTClient() {
    }

    public static Blaza_API get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://192.168.43.76/").addConverterFactory(GsonConverterFactory.create()).build();
        REST_CLIENT = retrofit.create(Blaza_API.class);
    }
}