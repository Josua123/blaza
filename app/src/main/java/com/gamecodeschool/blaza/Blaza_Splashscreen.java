package com.gamecodeschool.blaza;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.view.View;

public class Blaza_Splashscreen extends AppCompatActivity {
    Context myContext;
    ProgressDialog progress;
    Blaza_API Blaza_API;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blaza__splashscreen);

        ImageView image = (ImageView) findViewById(R.id.imageView);
        Animation animation1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade);
        image.startAnimation(animation1);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run(){
                final Intent mainIntent = new Intent(getApplicationContext(), Blaza_Home.class);
                startActivity(mainIntent);
                finish();

            }
        }, 1800);

        myContext = getApplicationContext();
        progress = ProgressDialog.show(Blaza_Splashscreen.this, "Inisialisasi Data", "Sedang Mengunduh Data Untuk Aplikasi", false);


        Callback<Blaza_ResponseError> hasil = new Callback<Blaza_ResponseError>() {
            @Override
            public void onResponse(Call<Blaza_ResponseError> call, Response<Blaza_ResponseError> response) {
                if (response.isSuccessful()) {
                    List<Blaza_Model> hasilHpBlaza = response.body().getHasil();
                    int jumlahData = response.body().getHasil().size();
                    if (jumlahData > 0) {
                        Blaza_Controller chb = new Blaza_Controller(myContext);
                        chb.open();
                        chb.deleteData();
                        for (int y = 0; y < jumlahData; y++) {
                            Blaza_Model tmpHasil = hasilHpBlaza.get(y);
                            chb.insertData(tmpHasil.getId(), tmpHasil.getNama(), tmpHasil.getMerk(), tmpHasil.getFoto(), tmpHasil.getHarga(), tmpHasil.getKeluaran());
                        }
                        chb.close();
                        Intent sendIntent = new Intent(myContext, Blaza_TerbaruFragment.class);
                        startActivity(sendIntent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "DATA SEDANG TIDAK TERSEDIA", Toast.LENGTH_LONG).show();
                    }
                    progress.dismiss();
                } else {
                    Log.e("onResponse failure", "Code: " + response.code() + " , Message: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<Blaza_ResponseError> call, Throwable t) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "AKSES KE SERVER GAGAL" + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        };

        Blaza_API= RESTClient.get();
        Call<Blaza_ResponseError> callHasil = Blaza_API.getHasil();
        callHasil.enqueue(hasil);
    }
}
