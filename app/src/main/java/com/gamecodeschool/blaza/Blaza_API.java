package com.gamecodeschool.blaza;

import retrofit2.Call;
import retrofit2.http.POST;

public interface Blaza_API {
    @POST("/blaza_ws/hasil")
    Call<Blaza_ResponseError> getHasil();
}
