package com.gamecodeschool.blaza;

import java.util.List;

public class Blaza_ResponseError {
    private String error;
    private List<Blaza_Model> hasil;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Blaza_Model> getHasil() {
        return hasil;
    }

    public void setHasil(List<Blaza_Model> hasil) {
        this.hasil = hasil;
    }
}
