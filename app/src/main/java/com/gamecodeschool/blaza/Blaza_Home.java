package com.gamecodeschool.blaza;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ViewFlipper;

public class Blaza_Home extends AppCompatActivity {
    ViewFlipper v_flipper;
    Button btnTerbaru, btnDiskon;
    Blaza_TerbaruFragment terbaru;
    Blaza_DiskonFragment diskon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blaza__home);

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ft = fm.beginTransaction();
        diskon = new Blaza_DiskonFragment();
        terbaru = new Blaza_TerbaruFragment();
        ft.add(R.id.frameLayout, terbaru);
        ft.addToBackStack("Terbaru");
        ft.commit();

        btnTerbaru = (Button) findViewById(R.id.terbaru);
        btnDiskon =(Button) findViewById(R.id.diskon);

        btnTerbaru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tombolTerbaru();
            }
        });

        btnDiskon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tombolDiskon();
            }
        });

        int image[] = {R.drawable.iklan1, R.drawable.iklan2, R.drawable.iklan3, R.drawable.iklan4, R.drawable.iklan5};
        v_flipper = findViewById(R.id.blazaSlider);

        for(int i=0; i<image.length; i++){
            fliverImage(image[i]);
        }
        for(int images : image)
            fliverImage(images);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.Btnberanda : {
                Intent nextScreen = new Intent(getApplicationContext(), Blaza_Home.class);
                startActivity(nextScreen);
                break;
            }
            case R.id.Btnlogin : {
                Intent nextScreen = new Intent(getApplicationContext(), Blaza_LoginActivity.class);
                startActivity(nextScreen);
                break;
            }
            case R.id.Btndaftar : {
                Intent nextScreen = new Intent(getApplicationContext(), Blaza_DaftarActivity.class);
                startActivity(nextScreen);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    void tombolTerbaru(){
        FragmentTransaction fTrans = getSupportFragmentManager().beginTransaction();
        terbaru = new Blaza_TerbaruFragment();
        fTrans.replace(R.id.frameLayout, terbaru);
        fTrans.commit();
    }

    void tombolDiskon(){
        FragmentTransaction fTrans = getSupportFragmentManager().beginTransaction();
        diskon = new Blaza_DiskonFragment();
        fTrans.replace(R.id.frameLayout, diskon);
        fTrans.commit();
    }

    //slider
    public void fliverImage(int images){
        ImageView imgView = new ImageView(this);
        imgView.setBackgroundResource(images);

        v_flipper.addView(imgView);
        v_flipper.setFlipInterval(1500);
        v_flipper.setAutoStart(true);
    }
}
