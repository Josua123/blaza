package com.gamecodeschool.blaza;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v7.widget.LinearLayoutManager;
import android.content.res.Resources;

import java.util.List;

public class Blaza_TerbaruFragment extends Fragment {
    ListView lstView;

    private static final class Holder{
        public TextView txtNama;
        public TextView txtHarga;
//        public ImageView imgFoto;
    }

    public Blaza_TerbaruFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blaza__terbaru, container, false);

        lstView = (ListView) view.findViewById(R.id.listView);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);


        Blaza_Controller myData = new Blaza_Controller(getActivity().getApplicationContext());
        myData.open();
        myData.getData();
        Blaza_Adapter adapter = new Blaza_Adapter(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, myData.getData());
        lstView.setAdapter(adapter);
        myData.close();

        return view;
    }

    private class Blaza_Adapter extends ArrayAdapter<Blaza_Model> {
        private LayoutInflater mInflater;

        public Blaza_Adapter(Context context, int textViewResourceId, List<Blaza_Model> objects) {
            super(context, textViewResourceId, objects);
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            Holder holder;
            Blaza_Model stream = getItem(position);
            if (view == null) {
                view = mInflater.inflate(R.layout.cardview_blaza__terbaru, parent, false);
                holder = new Holder();
                holder.txtNama = (TextView) view.findViewById(R.id.nama);
//                holder.imgFoto = (ImageView) view.findViewById(R.id.foto);
//                if(holder.imgFoto != null) {
//                    Resources res = getContext().getResources();
//                    String sIcon = "com.gamecodeschool.blaza:res/drawable/" + stream.getFoto();
//                    holder.imgFoto.setImageDrawable(res.getDrawable(res.getIdentifier(sIcon, null, null)));
//                }
                holder.txtHarga = (TextView) view.findViewById(R.id.harga);


                view.setTag(holder);
            } else {
                holder = (Holder) view.getTag();
            }

            holder.txtNama.setText(stream.getNama());
//            holder.imgFoto.setImageResource(stream.getFoto());
            holder.txtHarga.setText(stream.getHarga());

            return view;
        }
    }
}