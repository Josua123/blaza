package com.gamecodeschool.blaza;

public class Blaza_Model {
    private int id;
    private String nama;
    private String merk;
    private int foto;
    private String harga;
    private String keluaran;

    public Blaza_Model() {}

    public Blaza_Model(int id, String nama, String merk, int foto, String harga, String keluaran) {
        this.id = id;
        this.nama = nama;
        this.merk = merk;
        this.foto = foto;
        this.harga = harga;
        this.keluaran = keluaran;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getKeluaran() {
        return keluaran;
    }

    public void setKeluaran(String keluaran) {
        this.keluaran = keluaran;
    }
}
