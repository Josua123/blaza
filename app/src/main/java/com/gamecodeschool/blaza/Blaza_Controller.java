package com.gamecodeschool.blaza;

import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class Blaza_Controller {
    private Blaza_DBHelper dbHelper;
    private SQLiteDatabase database;
    public static final String TABLE_NAME = "blaza";
    public static final String ID = "id";
    public static final String NAMA = "nama";
    public static final String MERK = "merk";
    public static final String FOTO = "foto";
    public static final String HARGA = "harga";
    public static final String KELUARAN = "keluaran";

    public static final String CREATE_BLAZA = "CREATE TABLE "+TABLE_NAME+" "+"("+ID+" integer primary key, "+NAMA+" VARCHAR(100), "+MERK+" VARCHAR(100), " +FOTO+" VARCHAR(100), " +HARGA+" VARCHAR(100)," +KELUARAN+" VARCHAR(10))";

    private String[] TABLE_COLUMNS = {ID, NAMA, MERK, FOTO, HARGA, KELUARAN};

    public Blaza_Controller(Context context) {
        dbHelper = new Blaza_DBHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void deleteData (){
        database.delete(TABLE_NAME, null,null);
    }

    public void insertData(int id, String nama, String merk, int foto, String harga, String keluaran){
        ContentValues contentValues = new ContentValues();
        contentValues.put(ID, id);
        contentValues.put(NAMA, nama);
        contentValues.put(MERK, merk);
        contentValues.put(FOTO, foto);
        contentValues.put(HARGA, harga);
        contentValues.put(KELUARAN, keluaran);

        database.insert(TABLE_NAME, null, contentValues);
    }

    public ArrayList<Blaza_Model> getData() {
        ArrayList<Blaza_Model> allData = new ArrayList<Blaza_Model>();
        Cursor cursor = null;

        cursor = database.query(TABLE_NAME, TABLE_COLUMNS, null, null, null, null, ID + " DESC");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            allData.add(parseData(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        return allData;
    }

    private Blaza_Model parseData(Cursor cursor) {
        Blaza_Model curData = new Blaza_Model();
        curData.setId(cursor.getInt(0));
        curData.setNama(cursor.getString(1));
        curData.setMerk(cursor.getString(2));
        curData.setFoto(cursor.getInt(3));
        curData.setHarga(cursor.getString(4));
        curData.setKeluaran(cursor.getString(5));

        return curData;
    }
}
